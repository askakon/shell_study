#!/bin/bash
FILE=/home/kakon/Documents/cut.txt
  cut -d "-" -f 1,3 ${FILE}

cut -d "-"  -f 1,2 ${FILE}
 
cut -d "-" -f 1 ${FILE}

cut -b 1- ${FILE}
cut -b 3- ${FILE}
cut -b -4 ${FILE}
cut -c 2,5,7 ${FILE}
cut -c 1-7 ${FILE}
cut -c 1- ${FILE}
cut --complement -d "-" -f 1 ${FILE}
cut --complement -c 5 ${FILE}
cut -d "-" -f 1,2  ${FILE} --output-delimiter='|'
cut -d "-" -f 1,2  ${FILE} --output-delimiter='='
cut --version
cat  ${FILE} | cut -d "-" -f 1- | sort -r