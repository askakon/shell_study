#!/bin/bash

FILE=/home/kakon/Documents/practise/text_file.txt
sort ${FILE}
sort /home/kakon/Documents/test_05.txt > ${FILE}

sort -o  ${FILE} /home/kakon/Documents/sort_shell.txt 
sort -r  ${FILE}
sort -n  ${FILE}
sort -nr ${FILE}
sort -k 2n /home/kakon/Documents/sort_shell.txt 
sort -c /home/kakon/Documents/sort_shell.txt 
sort -u /home/kakon/Documents/sort_shell.txt
sort -M /home/kakon/Documents/practise/text_file_1.txt
