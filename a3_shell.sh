#!/bin/bash
num=1
rows=4
for((i=1; i<=rows; i++))
do
  for((j=1; j<=i; j++))
  do
    echo -n "$num "
    num=$((num + 1))
  done
  echo
done
