#!/bin/bash
h=$(date +"%H")
if [ $h -gt 5 -a $h -le 12 ]
then
echo good morning.Have a nice day! 
date `+DATE: %A-%m-%y%nTIME: %H:%M:%S `
elif [ $h -gt 12 -a $h -le 13 ]
then 
echo good afternoon.Have a nice day!  
date `+DATE: %A-%m-%y%nTIME: %H:%M:%S `
elif [ $h -gt 14 -a $h -le 17 ]
then
echo good evening.Have a nice day!  
date `+DATE: %A-%m-%y%nTIME: %H:%M:%S `
else
echo good night.Have a nice day!  
date `+DATE: %A-%m-%y%nTIME: %H:%M:%S `
fi